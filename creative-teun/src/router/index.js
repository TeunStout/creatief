import Vue from 'vue';
import Router from 'vue-router';

import Homepage from '@/components/Homepage';
import Japanese from '@/components/content/Japanese';
import Animation from '@/components/content/Animation';

Vue.use(Router);

export default new Router({
    routes: [{
        path: '/',
        name: 'homepage',
        component: Homepage
    }, {
        path: '/japanese',
        name: 'japanese',
        component: Japanese
    }, {
        path: '/animation',
        name: 'animation',
        component: Animation
    }, ]
});